# xTop, v2.1 - CHANGELOG
Changes in xTop 2.1 compared to v2.0  
Author: Matteo Mori  
Date: May 15th, 2024.  

xTop v2.1 is a minor update to v2.0 with a fix to the code ranking the peptides after the application of xTop. Previously, peptides could be sometimes ranked incorrectly, thus affecting the absolute quantification. This version also includes minor improvements to the figure produced for data analysis when `xTopSettings.plot_xTop_convergence` is set to True.