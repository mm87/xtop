function pepData = xTop(source_filename_arg)
% function pepData = xTop(source_filename_arg)
%
% This function loads one or more files with peptide precursor intensities,
% and generates one or more files with protein data, including the protein
% intensity (TopPepN, TopPepAll and/or xTop), peptide ranking and detection
% efficiencies, as well as other useful quantities.
%
% Input (optional):
%
% source_filename_arg     Filenames (relative or absolute paths) to the
%                         sources with peptide data (plain text csv format,
%                         see the README file for the specifications).
%                         If absent, the files specified by the variable
%                         [source_filename] are used instead. The filenames
%                         can be provided as a string (single file) or cell
%                         array of strings (single or multiple files).
% 
% Output: 
%
% pepData                 A matlab structure (or cell array of structures)
%                         containing informations on the peptide ranking.
%                         If multiple files are used and ranking is
%                         performed individually, then the program generates
%                         a cell array of structures, one per file.
%
% The output structure has the following fields:
%
% pepData.input_file       Input file(s)
% pepData.sample_name      The names of the samples (column headers in the
%                          source file(s))
% pepData.unique_pepPrec   Peptide precursor ID
% pepData.unique_protein   Protein ID
% pepData.pep2prot         Sparse matrix relating peptide precursors to proteins;
%                          The entries of the matrix are the peptide ranks.
% pepData.pepInt           Total intensity of each peptide across all samples        
% pepData.pepN             Number of samples in which each peptide
%                          precursor is detected
% pepData.pepMax           Maximum intensity of the peptide precursor across
%                          all samples  
%

global rankSettings;   % <- do not touch
global TopPepSettings; % <- do not touch
global xTopSettings;   % <- do not touch
global verbose;        % <- do not touch

%% SETTINGS ARE HERE BELOW % % % % % % % % % % % % % % % % % % % % % % % 

%% I/O SETTINGS %%%%
% [source_filename] Name (relative or absolute path) of the source file(s).
% These file names are ignored if file names are provided as arguments when
% running the program. In absence of input to the xTop function,
% this variable must correspond to a cell array of strings corresponding
% to paths to csv files.
%
% Example (only one file):
%   source_filename = {'my_peptide_intensities.csv'};
% Example (multiple files):
%   source_filename = {'my_file_1.csv';'my_file_2.csv';'my_file_3.csv'};
%
% Do not forget the file extension (usually .csv), unless the source files
% do not have one.
source_filename   = { 'test_file1.csv' , 'test_file2.csv' };

% [output_folder_name] If you don't like the default name of the export
% folder (with date and time), set here the name of the folder.
% The values '',[] or {} correspond to the default naming of the folder.
output_folder_name = [];

%% RANKING SETTINGS %%%%
% [rankFiles] The variable determines the behavior of the ranking if multiple
% input files are provided. It can take two values, 'individual' or 'all'.
%
% If rankFiles = 'individual', peptides in each set are ranked separately,
% and each ranking is used to compute the TopPepN protein intensity for
% each input file.
%
% If rankFiles = 'all', a unique ranking is generated, which is used to
% compute all protein intensities.
rankSettings.rankFiles = 'individual'; % 'individual' or 'all'

% Peptides are ranked according to the total intensity across the samples.
% A preliminary filtering can be performed to discard peptides with large
% total intensities, but detected in only a few conditions. [filterThres] sets
% the filter strength. It has to be a number between 0 and 1.
% This affects for conventional TopPepN and xTopN intensities.
%
% If filterThres=0, then no filtering is performed. 
% A value close to 0.5 can improve significantly TopPep1 intensities.
rankSettings.filterThres = 0.5;

% [export_ranking] If true, the program will generate files containing the
%                  ranking of the peptides used to compute
%                  TopPepN intensities. 
rankSettings.export_ranking = false; % true or false

% [problematicPeptides] Use this variable to exclude some peptides from the
%         computation of protein intensities (without editing the source file).
%         This is useful for the TopPepN approaches, in particular for low N;
%         xTop is considerably less impacted by noisy peptides than TopPepN,
%         so this is usually not necessary.
% 
%         The entries of this vector must be in the first column of the
%         input file. To keep all peptides, set problematicPeptides = {}.               
problematicPeptides = { };

%% TopPep-N SETTINGS %%%%
% Settings specific to the generation of TopPepN intensities.

% [TopPepVal] This numerical array sets which TopPepN intensities will be
%             generated, including TopPepAll that sums all peptide precursor
%             intensities. Examples:
%             TopPepVal = [];         % Leave empty to do nothing (default)
%             TopPepVal = [1];        % Generates TopPep1 
%             TopPepVal = [1 2 3];    % Generates TopPep1, TopPep2 and TopPep3
%             TopPepVal = [1 Inf];  % Generates TopPep1 and TopPepAll 
TopPepSettings.TopPepVal = [ ]; 

% [export_Npep] This flags sets whether the program exports to file the
%     number of TopPepN peptides detected. If set to true, the program will
%     generate files equal in number to the length of [TopPepVal].
%     For instance, if TopPepVal = [5 Inf], two files will be generated,
%     the first file containing how many of the top 5 peptides are detected
%     in each sample, and the second containing how many peptides are
%     detected in total for each protein.
TopPepSettings.export_Npep = false; % true or false


%% xTop SETTINGS %%%%
% Here below are the settings specific to the generation of xTop intensities.

% [xTopPepVal] This array sets the overall normalization of the xTop intensities.
% The default is [1] (i.e. the intensity of the top peptide).
% It can take the same values as the TopPepVal variable above.
% In particular, xTop intensities are not computed if this variable is set
% to an empty array, [].
xTopSettings.xTopPepVal = [ 1 ];

% The next important settings determine how xTop handles noise in the data
% and outliers.

% [thres_xTop] Peptide intensities below this value are ignored.
%              We also sets to zero all estimated xTop protein intensities
%              below this value. Use a small value eg. 300 to take care of
%              rounding error, and larger values to filter out "real data"
xTopSettings.thres_xTop = 300;

% [N_c_thres]  In order to work, xTop needs at least two peptides. If less peptides
%              are available, xTop returns the TopPep1 intensity. N_c_thres specifies
%              the minimum number of samples in which a peptide must be detected in
%              order to be used by xTop. It has to be at least two to get a
%              nonzero error for the peptide efficiencies. Default is three.
xTopSettings.N_c_thres = 3; % a number >=2

% [s2_min] Variance regularization coefficient. This parameter governs the
%          estimation of the uncertainties by setting the magnitude of the 
%          log-likelihood term X = s2_min/2 * sum_p sigma^(-2)_p.
%          If s2_min is set to zero, the uncertainty of one of the peptide
%          efficiencies will converge to zero, i.e. it will be assumed to be
%          known exactly. (Don't set it to zero.)
%
%          Suggested value: 0.01. Higher values might be set in presence of
%          high noise levels.
xTopSettings.s2_min = 0.01;

% [absolute_filter_ratio] This parameter determines which peptides are
% excluded for the purpose of absolute quantification. This avoids the xTop
% intensities being set to high values based on inaccurate or incorrectly
% identified peptide precursors. Note that s2 determines how much the
% peptide is weighted in the determination of the xTop intensities. The
% maximum weight is w_max = 1/s2_min.
% Set to x: Only peptides with s2<s2_min/x, where x is the value of the
% parameter, are considered for the absolute quantification.
% The top peptide satisfying this condition will provide the scaling for
% the xTop intensities.
%
% Set to zero (or a negative value): no filter
% Set to 1 (or greater): all peptides are filtered out (don't do it)
% If no peptides are left after the filtering, the absolute scale is
% determined by the top peptide, but the xTop error is set to the
% maximum, signalling unreliable absolute quantification. %%%% [CHECK]
%
% Suggested value: 0.1 (only filters out pretty extreme outliers)
xTopSettings.absolute_filter_ratio = 0.1;


% [plot_xTop_convergence] If true, it generates a summary plot for each optimization.
%            It is only used for debug/testing purposes, keep it to false
%            when analyzing large datasets. Only use when parallelize_flag=false
xTopSettings.plot_xTop_convergence = false;

% The next variables affect the optimization process and the possibility
% of parallelize the workload on multiple CPUs. By default, it uses all
% available cores. 

% [accuracy_max], [N_step_max], [N_optimizations]
%   These variables control the behavior of the iterative procedure to
%   obtain the MAP estimators. The optimization stops either when the
%   relative change in L (negative log likelihood) is less than
%   [accuracy_max], or when it has performed [N_step_max]
%   iterations. The optimization is repeated [N_optimizations]
%   times, and the solution with the highest likelihood is picked.
xTopSettings.accuracy_max = 1e-10;  % positive
xTopSettings.N_step_max = 1000; % integer larger than 0
xTopSettings.N_optimizations = 10; % integer larger than 0

% [rng_seed] xTop uses random number generation. Here, you can specify the
% seed (a nonegative integer less than 2^32) or use 'shuffle' to set it
% according to the computer clock (default).
xTopSettings.rng_seed = 'shuffle';

% [export_level] This sets whether only the basic output of xTop is
% generated (intensities only), or additional data is exported (e.g. peptide
% efficiencies, uncertainties etc.). Options: 'basic' or 'full'
xTopSettings.export_level = 'full';

% [parallelize_flag] If set to true, the computation of xTop protein
% intensities will be parallelized over each protein. This strongly reduces
% the computing time for large datasets. Options: true or false. 
xTopSettings.parallelize_flag = true;

% [max_parallel_workers] This is the maximum number of parallel workers.
% Set to a very large number, or to a negative one, to use all the
% available workers. Setting this value to 0 is equivalent to running the
% xTop calculations serially.
xTopSettings.max_parallel_workers = -1;


%% Other settings %%%%
%
% [verbose] Verbosity of the program. Values:
% 0    (silent)
% 1    (basic info)
% 2    (detailed info)
% 3    (a lot of info, including many details on xTop optimization).
verbose = 1;

% [strip_source_extension] If set to true, the source names in the output
% filenames won't have the extension (for instance .csv).
% Be careful with source files with no extension and dots in the name.
strip_source_extension = true;

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% END OF SETTINGS. You usually do not want to edit here below % 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

%% Checking inputs, printing settings on screen 

if exist('source_filename_arg','var')
    source_filename = source_filename_arg;
end
if ~iscell(source_filename) % if string, convert to cell
    source_filename = {source_filename};
end

Nfiles = length(source_filename); % How many input files?
if Nfiles==0
    error('Provide at least one file source.');
end

flag_no_source = false;
for k=1:Nfiles
    if ~exist(source_filename{k},'file') % How many of these exist?
        fprintf('Could not locate %s\n',source_filename{k});
        flag_no_source = true;
    end
end
if flag_no_source
    error('Fix the names of the source files.');
end

% If no custom foldername is provided, a default one is created
if isempty(output_folder_name)
    output_folder_name = ['xTop_output_',datestr(clock,'yy-mm-dd-HHMM')];
end

if ~exist(output_folder_name,'file')
    mkdir(output_folder_name);
elseif verbose
    fprintf(['** Warning: destination folder already exists. **\n',...
             '** Content will be overwritten.                **\n\n']);
end

for k=1:Nfiles
    
    if strip_source_extension
        % Strip extension from the source files
        temp = strsplit(source_filename{k},'.');
        if length(temp)==1
            fname_output{k} = temp{1};
        else
            fname_output{k} = strjoin(temp(1:end-1),'.');
        end
        fname_output{k} = [output_folder_name,'/[',fname_output{k},']'];
    else
        % Keep original file name
        fname_output{k} = [output_folder_name,'/[',source_filename{k},']'];
    end
    
end
switch rankSettings.rankFiles
    case 'all'
        fname_pepRanking = [output_folder_name,'/[GLOBAL] PepRank.csv']    ;    
    case 'individual'
        for k=1:Nfiles
            fname_pepRanking{k} = [fname_output{k},' PepRank.csv'];
        end
    otherwise
        error('The variable rankFiles must be either ''all'' or ''individual''.');            
end

if verbose
    fprintf('\n* * * Main Settings * *\n');
    fprintf('* Input files:\n');
    for k=1:Nfiles
        fprintf('*     %s\n',source_filename{k});
    end
    
    if ~isempty(problematicPeptides)
        fprintf('*\n* A list of %i peptides will be removed from the analysis:\n',numel(problematicPeptides));
        for k=1:min(numel(problematicPeptides),10)
            fprintf('*   %s\n',problematicPeptides{k});
        end
        if numel(problematicPeptides)>10
            fprintf('*   and %i more.\n',numel(problematicPeptides)-10);
        end
    end
    
    if Nfiles>1
        switch rankSettings.rankFiles
            case 'all'
                fprintf('*\n* Ranking will be performed on all data sources simultaneously.\n');
            case 'individual'
                fprintf('*\n* Ranking will be performed on each data source individually.\n');
        end
    end    
    if rankSettings.filterThres==0
        fprintf('*   Ranking mode: no filtering.\n');
    else
        fprintf('*   Ranking mode: filtered (threshold: %4.3f)\n',rankSettings.filterThres);
    end
    if rankSettings.export_ranking
        fprintf('*   Export ranking = true\n');
    else
        fprintf('*   Export ranking = false\n');
    end
    
    if ~isempty(TopPepSettings.TopPepVal)
        fprintf('*\n* TopPepN settings\n*   Intensities to be exported:\n');
        for k=1:length(TopPepSettings.TopPepVal)
            if ~isinf(TopPepSettings.TopPepVal(k))
                fprintf('*     TopPep%i\n',TopPepSettings.TopPepVal(k));
            else
                fprintf('*     TopPepAll\n');
            end
        end
        if TopPepSettings.export_Npep
            fprintf('*   Export Npep = true\n');
        else
            fprintf('*   Export Npep = false\n');
        end
    end
    
    fprintf('*\n* xTop settings.\n');
    fprintf('*            thres_xTop: %f\n',xTopSettings.thres_xTop);
    fprintf('*             N_c_thres: %i\n',xTopSettings.N_c_thres);
    fprintf('*                s2_min: %f\n',xTopSettings.s2_min);
    fprintf('* absolute_filter_ratio: %f\n',xTopSettings.absolute_filter_ratio);
    fprintf('*          accuracy_max: %e\n',xTopSettings.accuracy_max);
    fprintf('*            N_step_max: %i\n',xTopSettings.N_step_max);
    fprintf('*       N_optimizations: %f\n',xTopSettings.N_optimizations);
    
    switch xTopSettings.export_level
        case 'basic'
            fprintf('*\n* Only xTop intensities will be exported.\n');
        case 'full'
            fprintf('*\n* All xTop quantities will be exported.\n');
    end
    if ischar(xTopSettings.rng_seed)
        fprintf('* Random number generator seed: %s\n',xTopSettings.rng_seed);
    else
        fprintf('* Random number generator seed: %i\n',xTopSettings.rng_seed);
    end
    if xTopSettings.parallelize_flag
        if xTopSettings.max_parallel_workers==-1
            fprintf('* The xTop calculation will be parallelized using all available workers.\n');
        elseif xTopSettings.max_parallel_workers>0
            fprintf('* The xTop calculation will be parallelized using at most %i workers.\n',xTopSettings.max_parallel_workers);
        elseif xTopSettings.max_parallel_workers==0            
            fprintf('* The xTop calculation will not be parallelized.\n');
        end
    else
        fprintf('* The xTop calculation will not be parallelized.\n');
    end
        
    fprintf('* * * * * * * * * * * * * * * * * * * *\n');
end

%% Here below is where all the action happens
% tic; 
switch rankSettings.rankFiles
    case 'all'    
        if verbose
            fprintf('\n+ GLOBAL PEPTIDE RANKING\n');
        end
        pepData = rankPeptideLevels(source_filename);
        pepData = removeProblematicPeptides(pepData,problematicPeptides);
        
        for k=1:Nfiles
            exportCountsFromPep(pepData, source_filename{k}, fname_output{k}); 
        end
        
        if rankSettings.export_ranking
            exportPepRanking(pepData,fname_pepRanking);
        end
        
    case 'individual'
        for k=1:Nfiles
            if verbose
                fprintf('\n\n:::::::: SOURCE: %s\n',source_filename{k});
                fprintf('\n+ + + Peptide ranking\n');
            end
            
            pepData{k} = rankPeptideLevels(source_filename(k));
            pepData{k} = removeProblematicPeptides(pepData{k},problematicPeptides);
            exportCountsFromPep(pepData{k}, source_filename{k}, fname_output{k});         
            
            if rankSettings.export_ranking            
                exportPepRanking(pepData{k},fname_pepRanking{k}); 
            end
            
        end 
        
end

%% End of the program.
%x = toc;
%if verbose; fprintf('\nDone! Elapsed time: %im %4.3fs\n',floor(x/60),mod(x,60)); end

end



function exportCountsFromPep(pepData,f_in,f_out)
% This function adapts the pep2prot matrix to the proteins and peptides
% present in this dataset

global verbose;
global TopPepSettings; 
global xTopSettings;

X = importdata(f_in);
data_samples = X.textdata(1,3:end);
data_pepList = X.textdata(2:end,1);
data_protList = X.textdata(2:end,2);

Ns = length(data_samples);
    
% Here we extract the protein ID and protein name
data_unique_prot = unique(data_protList);
Np = length(data_unique_prot);

if verbose
    fprintf('\n* * * Protein intensities\n* Output:   %s\n',f_out);
end

for pi=1:Np
    % For each protein, get the corresponding protein in the global set  
    pj = find(strcmp(pepData.unique_protein,data_unique_prot{pi}));
    
    % get the associated peptides and ranks
    qj = find(pepData.pep2prot(:,pj));
    pepName{pi} = pepData.unique_pepPrec(qj);
    pepRank{pi} = full(pepData.pep2prot(qj,pj)); % rank
    
    maxRank = max(pepRank{pi});
    ii = pepRank{pi}==0;
    pepRank{pi}(ii) = maxRank:(maxRank+sum(ii));
end

% Here we compute and export the TopPepN quantities
for xi=1:length(TopPepSettings.TopPepVal)    
    if ~isinf(TopPepSettings.TopPepVal(xi))
        str_x = ['TopPep',num2str(TopPepSettings.TopPepVal(xi))];
    else
        str_x = 'TopPepAll';
    end
    if verbose
        fprintf('*   %s intensity ... ',str_x);
    end
    
    % Msig is the matrix of protein intensities
    M_TopPepN = zeros(Np,Ns);
    % Mpep is the number of peptide precursors detected for each protein
    M_Npep = zeros(Np,Ns);  
    for pi=1:Np
        % From the previous lists, get the local indexes (qi)
        % and the ultralocal index (qk, within the subset of peptides
        % associated to the protein)
        [~,qi,qk] = intersect(data_pepList,pepName{pi});
        ri = pepRank{pi}(qk);
        
        % select those with rank < thresh
        M_TopPepN(pi,:) =  sum(X.data(qi( ri<=TopPepSettings.TopPepVal(xi) ) , :)  , 1);        
        M_Npep(pi,:) =  sum(X.data(qi( ri<=TopPepSettings.TopPepVal(xi) ) , :)>0, 1);
    end
        
    % Export intensities
    f_out_x = [f_out,' Intensity ',str_x,'.csv'];
    
    M2 = cell(1,Ns+1);
    M2{1} = data_unique_prot;
    form = {'%s'};
    for s=1:Ns
        M2{1+s} = M_TopPepN(:,s);
        form{1+s} = '%i';
    end
    header = [{'Protein'},data_samples];
    export_dlm_data(f_out_x,M2,'header',header,'format',form,'delimiter',',');
    
    if TopPepSettings.export_Npep
        
        if verbose
            fprintf('Npep ...');
        end
        
        % Export number of peptides detected
        f_out_x = [f_out,' Npep ',str_x,'.csv'];
        
        M2 = cell(1,Ns+1);
        M2{1} = data_unique_prot;
        form = {'%s'};
        for s=1:Ns
            M2{1+s} = M_Npep(:,s);
            form{1+s} = '%i';
        end
        header = [{'Protein'},data_samples];
        export_dlm_data(f_out_x,M2,'header',header,'format',form,'delimiter',',');        
        
    end    
    
    if verbose
        fprintf(' ok\n');
    end
end

%%%%%% Here we compute and export xTop intensities
if ~isempty(xTopSettings.xTopPepVal)
    if verbose==1
        fprintf('*   xTop ... ');
    elseif verbose>1
        fprintf('* * * xTop protein intensities * * *\n');
        fprintf('Detailed info on the calculation of xTop intensities will be printed out.\n\n');
    end
    M_I_xTop = zeros(Np,Ns);
    M_log_I_xTop_var = zeros(Np,Ns);
    [M_pep_rank,M_pep_detections,M_pep_weight,M_eff,M_log_eff_var,...
        M_relQuant_valid , M_absQuant_valid] = deal(zeros(length(data_pepList),1));
    
    Veff = zeros(Np,length(xTopSettings.xTopPepVal)); 
    Veff_s2 = zeros(Np,length(xTopSettings.xTopPepVal)); 
    
    % protein-specific stats/output        
    xTop_stats.status = [];   
    xTop_stats.opt_idx = [];  
    xTop_stats.N_step = [];
    xTop_stats.N_detected = [];
    xTop_stats.N_valid_peptides = [];
    xTop_stats.L = []; 
    xTop_stats.L_optimal = []; 
    xTop_stats.L_range = [];  
    xTop_stats.N_local_minima = []; 
    stats_fields_2 = fields(xTop_stats);
    
    % global stats/settings
    xTop_stats.settings = xTopSettings;
    xTop_stats.N_proteins = Np;
    xTop_stats.N_optimizations = xTopSettings.N_optimizations;
    
    % Parallel computation setup.
    if xTopSettings.parallelize_flag
        avail_pool = gcp('nocreate');
        if isempty(avail_pool)
            avail_pool = gcp;
        end
        if xTopSettings.max_parallel_workers==-1
            N_parpool_workers = avail_pool.NumWorkers;
        else
            N_parpool_workers = min(xTopSettings.max_parallel_workers , avail_pool.NumWorkers);
        end
    else
        N_parpool_workers = 0;
    end    
    xTopSettings_local = xTopSettings;
    verbose_local = verbose;
    xTop_stats = repmat({xTop_stats},Np,1);
    [par_qi , par_pep_rank, par_pep_detections, par_pep_weight , ...
        par_eff , par_log_eff_var , relQuant_valid , absQuant_valid ] = deal(cell(Np,1));
    
    % To get the number of workers: p = gcp; p.NumWorkers
    parfor ( pi=1:Np , N_parpool_workers ) 
        
        % Get the local index (qi) for the peptides
        [~,qi,qk] = intersect(data_pepList,pepName{pi});
        par_qi{pi} = qi;
        
        % v2.1_test: removed preliminary filtering
        
        % Get xTop intensities and all other quantities
        [xTop_output,xTop_wrapper_output] = get_xTop_estimator_wrapper(X.data(qi,:),xTopSettings_local,verbose_local);
        M_I_xTop(pi,:) = xTop_output.I_xTop;
        M_log_I_xTop_var(pi,:) = xTop_output.log_I_xTop_var;
        par_pep_weight{pi} = xTop_output.pep_weight;
        par_eff{pi} = xTop_output.eff;
        par_pep_detections{pi} = sum( X.data(qi,:) >0 ,2);
        par_log_eff_var{pi} = xTop_output.log_eff_var;
        relQuant_valid{pi} = xTop_output.relQuant_valid_flag;
        absQuant_valid{pi} = xTop_output.absQuant_valid_flag;
        
        % Collect results for analysis        
        for fi=1:length(stats_fields_2);
            f = stats_fields_2{fi};
            xTop_stats{pi}.(f) = xTop_wrapper_output.(f);
        end
    end
    
    % Sorting and ranking. This is done in a separate loop to allow for the reindexing of X.data and data_pepList
    Npepval = length(xTopSettings_local.xTopPepVal);  
    for pi=1:Np  
        
        % Sort peptides by decreasing xTop efficiencies.
        % Note that the peptide IDs are not reordered; that will be done when printing out the data
        [~,j] = sort(par_eff{pi},'descend');
        
        % Sorting the peptides in the global data structure       
        qi = par_qi{pi};    
        X.data(qi,:) =  X.data(qi(j),:);
        data_pepList(qi) = data_pepList(qi(j)); 
             
        % Sorting data within the xTop results
        par_pep_weight{pi} = par_pep_weight{pi}(j);
        par_eff{pi} = par_eff{pi}(j);
        par_pep_detections{pi} = par_pep_detections{pi}(j);
        par_log_eff_var{pi} = par_log_eff_var{pi}(j);
        relQuant_valid{pi} = relQuant_valid{pi}(j);
        absQuant_valid{pi} = absQuant_valid{pi}(j); 
               
        par_pep_rank{pi} = (1:length(par_eff{pi}));
        
        % Sort by rank in order to calculate the xTop intensities
        par_pep_rank_adjusted_sorted = cumsum(absQuant_valid{pi}');
        %[ X.data(qi,1)/1e6 , par_eff{pi} , par_pep_rank{pi}' , relQuant_valid{pi}' , absQuant_valid{pi}' , par_pep_rank_adjusted_sorted ] 
        
        % Calculate the xTop intensities: sum over the first tp efficiencies for which absQuant_valid is true
        [Veff_vec,Veff_s2_vec] = deal( zeros(Npepval,1) );
        for xi=1:Npepval      
            i_sel = absQuant_valid{pi}' & par_pep_rank_adjusted_sorted<=xTopSettings_local.xTopPepVal(xi);
            ep = par_eff{pi}(i_sel);            
            Veff_vec(xi) = sum(ep);
            V_x = par_log_eff_var{pi}(i_sel);
            Veff_s2_vec(xi) = sum( (ep.^2 .* V_x) ./ (Veff_vec(xi)^2)' ); % sum over the first tp valid efficiencies
        end
        Veff(pi,:) = Veff_vec;
        Veff_s2(pi,:) = Veff_s2_vec;        
    end
    
    % Export
    for xi=1:length(xTopSettings.xTopPepVal) 
        if ~isinf(xTopSettings.xTopPepVal(xi))
            str_x = num2str(xTopSettings.xTopPepVal(xi));
        else
            str_x = 'All';
        end
        f_out_x1 = [f_out,' Intensity xTop',str_x,'.csv'];
        f_out_x2 = [f_out,' Error xTop',str_x,'.csv'];
        [M2,M3] = deal(cell(1,Ns+1));
        [M2{1},M3{1}] = deal(data_unique_prot);
        form2 = {'%s'};
        form3 = {'%s'};
        for s=1:Ns
            M2{1+s} = floor(Veff(:,xi) .* M_I_xTop(:,s)); % we export integers, we do not need decimals
            M3{1+s} = floor( (Veff(:,xi) .* M_I_xTop(:,s)) .* sqrt( Veff_s2(:,xi) + M_log_I_xTop_var(:,s) ) );
            form2{1+s} = '%i';
            form3{1+s} = '%i';
        end
        header = [{'Protein'},data_samples];
        export_dlm_data(f_out_x1,M2,'header',header,'format',form2,'delimiter',','); % protein intensities
        switch xTopSettings.export_level
            case 'full'
                export_dlm_data(f_out_x2,M3,'header',header,'format',form3,'delimiter',','); % errors
            case 'basic'
                % nothing
            otherwise
                error('export_level is set to neither ''basic'' nor ''full'' (but instead to %s).',xTopSettings.export_level);
        end
    end
    
    switch xTopSettings.export_level
        case 'full'
            
            % Put together the peptide-specific info
            for pi=1:Np
                qi = par_qi{pi};
                M_pep_rank(qi) = par_pep_rank{pi};
                M_pep_detections(qi) = par_pep_detections{pi};
                M_pep_weight(qi) = par_pep_weight{pi};
                M_eff(qi) = par_eff{pi};
                M_log_eff_var(qi) = par_log_eff_var{pi};
                M_relQuant_valid(qi) = relQuant_valid{pi};
                M_absQuant_valid(qi) = absQuant_valid{pi};
            end
            
            % Remove peptides with zero detections
            i_exp = M_pep_detections>0;
            
            % Export peptide data
            f_out_x = [f_out,' xTop peptide statistics.csv'];
            Mpep = cell(1,9);
            
            
            Mpep{1} = data_pepList(i_exp);
            Mpep{2} = data_protList(i_exp);
            Mpep{3} = M_pep_rank(i_exp);
            Mpep{4} = M_pep_detections(i_exp);
            Mpep{5} = M_relQuant_valid(i_exp);
            Mpep{6} = M_absQuant_valid(i_exp);
            Mpep{7} = M_pep_weight(i_exp);
            Mpep{8} = M_eff(i_exp);
            Mpep{9} = M_log_eff_var(i_exp);
            form = {'%s','%s','%i','%i','%i','%i','%f','%f','%f'};
            header = {'Peptide','protein','pep_rank','pep_detections','relQuant_isValid','absQuant_isValid','pep_weight','eff','log_eff_var'};
            export_dlm_data(f_out_x,Mpep,'header',header,'format',form,'delimiter',',');
        case 'basic'
            % do nothing
        otherwise
            error('export_level is set to neither ''basic'' nor ''full'' (but instead to %s).',xTopSettings.export_level);
    end
    
    if verbose==1        
        fprintf('ok\n');
    elseif verbose>1
        fprintf('\nxTop intensities computed.\n');
    end
    
    
    % Export stats matlab structure
    f_out_x = [f_out,' xTop stats.mat'];
    save(f_out_x,'xTop_stats');
end



end

function pepData = removeProblematicPeptides(pepData,pepList)

global verbose;

cntRemoved = 0;

if ~isempty(pepList)    
    Nq = length(pepList);
    if verbose==1
        fprintf('\n* Removing problematic peptide(s)...\n');
    end
    if verbose>1
        fprintf('\n* Removing problematic peptide(s):\n');
    end
    for q = 1:Nq
        i = find(ismember(pepData.unique_pepPrec,pepList{q}));
        p = find(pepData.pep2prot(i,:));
        
        if isempty(p)
            if verbose>1
                fprintf('  [%i] %s\n    not found\n\n',i,pepData.unique_pepPrec{i});
            end
        else
            if verbose>1
                fprintf('  [%i] %s\n    associated to %s\n\n',i,pepData.unique_pepPrec{i},pepData.unique_protein{p});
            end
            
            % update ranks
            ri = full(pepData.pep2prot(i,p));
            jj_up = find(pepData.pep2prot(:,p) > ri);
            pepData.pep2prot(jj_up,p) = pepData.pep2prot(jj_up,p) -1;
            
            % remove peptide
            pepData.unique_pepPrec(i) = [];
            pepData.pep2prot(i,:) = [];
            pepData.pepInt(i) = [];
            pepData.pepN(i) = [];
            pepData.pepMax(i) = [];
            
            cntRemoved = cntRemoved + 1;
        end
        
        
    end
    
    if verbose
        fprintf('*   %i out of %i removed.\n',cntRemoved,Nq);
    end
end

end


function pepData = rankPeptideLevels(f_in)
% function pepData = rankPeptideLevels(f_in)
%
% This function reads the files containing the raw peptide counts and generates
% peptide-to-protein matrices with ranking according to average signal.
% The output structure is the input of the xTop_pipeline function.
%
% Fields of the output structure:
% pepData.sample_name
% pepData.pep_info (each row of the input data, including peptide sequence and charge)
% pepData.unique_protein (name of protein(s) identifier)
% pepdata.pep2prot (sparse matrix; entries are ranks)
%
% % % % % % Example of how to use the output
%
% Name of 190-th protein
% protName = pepData.unique_protein{190}
%
% Selection of top3 peps for protein 190
% idxpep = find(pepData.pep2prot(:,190))
% top3pep = find(pepData.pep2prot(:,190) > 0 & pepData.pep2prot(:,190) <= 3)

global verbose;
global rankSettings;
       

if ~exist('f_in','var')
    error('Input file is missing.');
end
if ~iscell(f_in)
    error('Input must be a cell array of input files.');
end

% How many input files?
nF = length(f_in);

%% Step 1: get full list of protein and peptides from all datasets
pepData.input_file = f_in;

for k=1:length(f_in)
    X = importdata(f_in{k});
    pepData.X{k}.filename = f_in{k};
    [nPep,nSamples] = size(X.data);    
    pepData.X{k}.sample_name = X.textdata(1,3:end); % changed in v2.1
    pepData.X{k}.pep_info = X.textdata(2:end,1);
    pepData.X{k}.prot_info = X.textdata(2:end,2);
    [pepData.X{k}.unique_protein,~,pepData.X{k}.pvec_all2unique] = unique(X.textdata(2:end,2));
    
    pepData.X{k}.pepSig = X.data;
    
    nProt = length(pepData.X{k}.unique_protein);
    
    if verbose
        if nF>1
            fprintf('+\n+ + + Source file: %s + + +\n',f_in{k});
        end
        fprintf('+  Number of samples: %i\n',nSamples);
        fprintf('+  Input number of peptides: %i\n',nPep);
        fprintf('+  Input number of protein assignations (including ambiguous): %i\n',nProt);
        
    end
end

%% Step 2: merge dataset (selecting unique peptides)
if verbose && nF>1
    fprintf('+\n+ + + Merging datasets + + +\n');
end

s = zeros(nF,2);
all_pep = cell(0,1);
all_prot = cell(0,1);
all_cond = cell(1,0);
for k=1:nF
    s(k,:) = size(pepData.X{k}.pepSig) ;
    all_pep = [all_pep ; pepData.X{k}.pep_info ];
    all_prot = [all_prot ; pepData.X{k}.prot_info ];
    all_cond = [all_cond , pepData.X{k}.sample_name ];
end

% Unique peptides
[upep,u2apep,a2upep] = unique(all_pep,'first');
pepData.unique_pepPrec = upep;
Nupep = length(upep);
all_prot_2 = all_prot(u2apep); % proteins corresponding to unique peptides

% Pep offset
pep_off = 0;
for k=1:nF
    pep_off(1+k) = pep_off(k) + length(pepData.X{k}.pep_info) ;
end

pepIntSum = zeros(Nupep,1); % total intensity across all samples
pepN = zeros(Nupep,1); % number of samples with nonzero intensities
pepMax = zeros(Nupep,1); % maximum intensity across samples
for k=1:nF   % for each file 
    for pi=1:length(pepData.X{k}.pep_info) % for each peptide
         % for each peptide, add counts to unique pep
         pu = a2upep(pep_off(k) + pi);
         pepIntSum(pu) = pepIntSum(pu) + full(sum(pepData.X{k}.pepSig(pi,:)));
         pepN(pu) = pepN(pu) + full(sum(pepData.X{k}.pepSig(pi,:)>0));
         pepMax(pu) = max([pepMax(pu) , full(pepData.X{k}.pepSig(pi,:))]);
    end
end

% unique proteins  
[pepData.unique_protein,~,pvec_all2unique] = unique(all_prot_2);
nProt = length(pepData.unique_protein);
if verbose && nF>1
    fprintf('+  Total number of samples: %i\n',length(all_cond));
    fprintf('+  Unique peptides: %i\n',Nupep);
    fprintf('+  Unique proteins: %i\n',nProt);
end
pepData.pep2prot = sparse((1:Nupep)',pvec_all2unique,ones(Nupep,1),Nupep,nProt);

%% Step 3: ranking
if verbose
    if rankSettings.filterThres==0
        fprintf('+  Ranking peptides according to total intensity... ');
    else
        fprintf('+  Filtering and ranking (filterThres = %3.2f)... ',rankSettings.filterThres); 
    end
end
pepData.pepInt = pepIntSum;
pepData.pepN = pepN; % only used when rankSettings.filterThres>0
pepData.pepMax = pepMax; % only used when rankSettings.filterThres>0

cntProtCut = 0; % only used when rankSettings.filterThres>0
cntPepCut = 0; % only used when rankSettings.filterThres>0
if rankSettings.filterThres==0
    
    for pi=1:nProt
        % For each protein, get the peptides
        v_pep = find(pepData.pep2prot(:,pi));
        
        % total intensity of each peptide
        top_pep_int = pepIntSum(v_pep);
        
        % Sort by descending average intensity and assign rank
        [~,ii] = sort(top_pep_int,'descend');
        
        for k=1:length(ii)
            pepData.pep2prot(v_pep(ii(k)),pi) = k;
        end
    end
    
else

    for pi=1:nProt
        
        % For each protein, get the peptides
        v_pep = find(pepData.pep2prot(:,pi));
        
        % total intensity of each peptide
        top_pep_int = pepIntSum(v_pep);
        top_pep_N = pepN(v_pep);
        
        [~,iN] = sort(top_pep_N,'descend'); % sort by number of conditions
        Nmax = top_pep_N(iN(1)); %iN(1) is the peptide detected in most conditions, Nmax is the maximum number of conditions
        
        % Sort by descending average intensity (rank: ii)
        [~,ii] = sort(top_pep_int,'descend');
        
        % Flag outliers for removal
        avg_pep_int = pepIntSum(v_pep) ./ (pepN(v_pep)+1e-8);
        avg_sign_ref = avg_pep_int(iN(1)); % average intensity of the peptide detected in most conditions
        usePep = true(length(top_pep_int),1);
        usePep( (top_pep_N < rankSettings.filterThres*Nmax) & (avg_pep_int > avg_sign_ref) ) = false;
                
        kx = 0;
        ii2 = zeros(length(ii),1);
        for ki=1:length(ii)
            k = ii(ki);
            if usePep(k)
                kx = kx+1;
                ii2(ki) = kx;
                pepData.pep2prot(v_pep(k),pi) = kx;
            else
                ii2(ki) = 0;
                pepData.pep2prot(v_pep(k),pi) = 0;
            end
        end
        if sum(usePep)<length(usePep)
            cntProtCut = cntProtCut + 1;
            cntPepCut = cntPepCut + length(usePep) - sum(usePep);
        end
    end
end

% Remove temporary field
pepData = rmfield(pepData,'X');
if verbose
    fprintf('ok\n');
    if rankSettings.filterThres>0
        fprintf('+  -> Peptides filtered out: %i\n+  -> Proteins affected:  %i\n',cntPepCut,cntProtCut);
    end
end

end

function exportPepRanking(X,fname)
% For each protein, print peptides with rank and infos

if iscell(fname)
    error('fname must be a string.');
end

[~,Nprot] = size(X.pep2prot);
fid = fopen(fname,'w');

for a=1:Nprot
    protName = X.unique_protein{a};
    [ii,~,pep_rank] = find(X.pep2prot(:,a));
    [~,s] = sort(pep_rank,'ascend');
    for k=1:length(ii)
        b = ii(s(k));
        pepName = X.unique_pepPrec{b};
        fprintf(fid,'%s,%s,%i\n',protName,pepName,k);
    end
end

fclose(fid);

end

function [xTop_output,xTop_wrapper_output] = get_xTop_estimator_wrapper(I_peptides,xTopSettings,verbose)
% function [xTop_output,xTop_wrapper_output] = get_xTop_estimator_wrapper(I_peptides,xTopSettings,verbose)
%
% This function calls get_xTop_estimator several times and picks the best solution

if (xTopSettings.N_optimizations)==1
    % Just call the main function once
    xTop_output = get_xTop_estimator(I_peptides,xTopSettings,verbose);
    result_status = xTop_output.optimization_status;
    result_L = xTop_output.L_optimal;
    L_optimal = xTop_output.L_optimal;
    result_N_step = xTop_output.N_step;
    result_opt_idx = 1;
    Delta_L = 0;
else
    % Call the main function several times, and return the solution with
    % maximum likelihood
    if verbose>1
        fprintf(' * * * N_optimizations = %i:\n',xTopSettings.N_optimizations);
    end
    result_status = zeros(xTopSettings.N_optimizations , 1);
    result_L = zeros(xTopSettings.N_optimizations , 1);
    result_N_step = zeros(xTopSettings.N_optimizations , 1);
    xTop_output_loop = cell(1,xTopSettings.N_optimizations);
    for q=1:(xTopSettings.N_optimizations)
        xTop_output_loop{q} = get_xTop_estimator(I_peptides,xTopSettings,verbose);
        result_status(q,1) = xTop_output_loop{q}.optimization_status;
        result_L(q,1) = xTop_output_loop{q}.L_optimal;
        result_N_step(q,1) = xTop_output_loop{q}.N_step;
    end
    L_optimal = min(result_L);
    Delta_L = result_L-L_optimal;
    
    % Pick minimum of Delta_L
    [~,result_opt_idx] = min(Delta_L);
    xTop_output = xTop_output_loop{result_opt_idx};
end

% Store variables
xTop_wrapper_output.status = result_status;
xTop_wrapper_output.opt_idx = result_opt_idx;
xTop_wrapper_output.N_detected = xTop_output.N_detected;
xTop_wrapper_output.N_valid_peptides = xTop_output.N_valid_peptides;
xTop_wrapper_output.N_step = result_N_step;

xTop_wrapper_output.L = result_L;
xTop_wrapper_output.L_optimal = L_optimal;
xTop_wrapper_output.L_range = max(Delta_L);

xTop_wrapper_output.N_local_minima = sum( (result_L-min(result_L) > 1e2*xTopSettings.accuracy_max*abs(L_optimal) ) ) ;
if verbose>1 && (xTopSettings.N_optimizations>1)
    fprintf('---> max(L)-min(L) = %f\n\n',xTop_wrapper_output.L_range);
end
    
end


function xTop_output = get_xTop_estimator(I_peptides,xTopSettings,verbose)
% function xTop_output = get_xTop_estimator(I_peptides,xTopSettings,verbose)
%
% This is the main function calculating the xTop estimator.
% The function returns a structure including the xTop intensities.
% It also includes additional outputs, including peptide efficiencies and estimated
% errors for the individual peptides. These are not printed on file in the current
% version of the code, but can be visualized settings verbose=3 (use only with
% small input files)

[Npep,Nc] = size(I_peptides);
eff = zeros(Npep,1); % inferred peptide detection efficiency
log_eff_s2 = zeros(Npep,1); %  Propagated uncertainty on the efficiencies (not real error bars though)
s2_pep = zeros(Npep,1); % Estimated peptide variance (note: it often depends on s2_min)
I_xTop = zeros(1,Nc); % xTop protein intensities
log_I_xTop_s2 = zeros(1,Nc); % Propagated uncertainty on the intensities (not real error bars though)

% How many of these peptides are effectively detected in at least N_c_thres conditions?
valid_peptide_flag = false(1,Npep);
conditions4peptide = zeros(1,Npep);
for k=1:Npep
    conditions4peptide(k) = sum(I_peptides(k,:)>xTopSettings.thres_xTop);
    valid_peptide_flag(k) = conditions4peptide(k) >= xTopSettings.N_c_thres;
end
I_peptides( ~valid_peptide_flag ,:) = 0;

% The following condition removes stray peptides that are detected only
% in conditions in which all other peptides are NOT detected.
isDetected = I_peptides>0;
peptidesInSample   = sum( isDetected , 1);    
isolatedPeptidesPresent = false;
if max(peptidesInSample)>1
    % how many other peptides are detected when pep. #k is detected?
    % If the answer is zero, and there are other peptides, this is likely
    % just noise.
    otherPeptidesDetected = zeros(1,Npep);
    for k=1:Npep
        s_idx = isDetected(k,:);
        if sum(s_idx)>0        
            otherPeptidesDetected(k) = max( peptidesInSample(s_idx) ) - 1;
        end
    end
    isolatedPeptide = valid_peptide_flag & (otherPeptidesDetected==0);
    if sum(isolatedPeptide)<max(peptidesInSample)
        valid_peptide_flag( isolatedPeptide ) = false;
        I_peptides( isolatedPeptide ,:) = 0;
    else
        % Keep the peptides, but calculate the protein intensities just by
        % summing the peptide intensities
        isolatedPeptidesPresent = true;
    end
end





Npep_nz = sum(valid_peptide_flag);

% Only get the conditions in which at least one valid peptide is detected:
valid_condition_flag = false(1,Nc);
peptides4condition = zeros(1,Nc);
validPeptides4condition = zeros(1,Nc);
for k=1:Nc
    peptides4condition(k) = sum( I_peptides(:,k) > max(xTopSettings.thres_xTop,0) );
    validPeptides4condition(k) = sum( I_peptides(valid_peptide_flag,k) > max(xTopSettings.thres_xTop,0) );
    valid_condition_flag(k) = (validPeptides4condition(k) > 0);
end
Nc_nz = sum(valid_condition_flag);

N_param = 2*Npep_nz + Nc_nz; % one efficiency and one variance per peptide, plus the valid conditions
N_detected = sum(sum( I_peptides(valid_peptide_flag,valid_condition_flag)>0 ));

%% Data structure with output variables
% General info and peptide info
xTop_output.Nc = Nc;
xTop_output.N_valid_conditions = Nc_nz;
xTop_output.N_valid_peptides = Npep_nz;
xTop_output.valid_peptide_flag = valid_peptide_flag;
xTop_output.valid_condition_flag = valid_condition_flag;
xTop_output.N_detected = N_detected;

% Results
xTop_output.I_xTop = zeros(1,Nc);
xTop_output.log_I_xTop_var = zeros(1,Nc);
xTop_output.eff = zeros(1,Npep);
xTop_output.log_eff_var = zeros(1,Npep);
xTop_output.pep_weight = zeros(1,Npep);
xTop_output.relQuant_valid_flag = false(1,Npep);
xTop_output.absQuant_valid_flag = false(1,Npep);

% Optimization behavior
xTop_output.optimization_status = 0;
xTop_output.s2_init = [];
xTop_output.N_step = 0;
xTop_output.L = 0;
xTop_output.L_optimal = 0;
xTop_output.diff_L = 0;
xTop_output.L_min_range = 0;

% for debug/analysis purposes
xTop_output.analysis{1} = [];
xTop_output.analysis{2} = [];
xTop_output.analysis{3} = [];
xTop_output.analysis{4} = [];

%%%% Initialization of random number generator.

% To randomize the output, keep rng('shuffle').
% Otherwise use a seed, like for instance rng(1).
xTop_output.rng_init_stats = rng(xTopSettings.rng_seed);



% xTop intensities can be computed only if the number of detected peptides
% is larger than N_param (Nc_nz xTop intensities, N_pep_nz efficiencies and
% Npep_nz variances). In particular, at least two peptides have to be
% detected across conditions: since N_detected is at most Npep_nz*Nc_nz,
% the condition N_param>N_detected leads to Nc_nz>4 when Npep_nz=2 and
% Nc_nz>3 when Npep_nz=3.

if Npep_nz==0
    % No peptides -> No intensity (defaulted to zero)
    
    xTop_output.optimization_status = 0;
    
    if verbose>2
        fprintf('No valid peptides.\n');
    end
    return
elseif Npep_nz==1
    % In this case, xTop intensity equals the intensity of the only peptide
    % available.

    xTop_output.optimization_status = -1;
    xTop_output.I_xTop = I_peptides(valid_peptide_flag,:); 
    
    if verbose>2
        fprintf('Only 1 valid peptide, or peptides detected in different conditions.\n');
    end
    return
elseif isolatedPeptidesPresent
    % Peptides are only detected in non-overlapping samples.
    % In this case, the xTop intensity equals to the sum of the intensities
    % of the peptides.

    xTop_output.optimization_status = -2;
    xTop_output.I_xTop = sum(I_peptides(valid_peptide_flag,:),1); 
    
    if verbose>2
        fprintf('Only 1 valid peptide, or peptides detected in different conditions.\n');
    end
    return
elseif N_param > N_detected    
    % Case relevant to when using xTop across 2 or 3 samples
    xTop_output.optimization_status = -3;
    xTop_output.I_xTop = max(I_peptides(valid_peptide_flag,:),[],1);
    
    if verbose>2
        fprintf('Insufficient data\n');
    end    
    return
end

%% Setup matrixes A and B
% We will fit log(M_ki) = log(epsi_k) + log(M_est_i) 
%
% First, reshape the data from a 2-dimensional matrix with missing entries
% to a 1-dimensional (column) vector.

Nq = Npep_nz + Nc_nz; % q= 1,..., Npep_nz matches the peptide efficiencies;
                      % q=Npep_Nz+1,...,Nq matches the xTop intensities
Nr = Npep_nz * Nc_nz; % Maximum number of entries in the peptide intensity matrix

y = reshape(I_peptides(valid_peptide_flag,valid_condition_flag),Nr,1);  % experimental data (only peptides and conditions to be included in the analysis)
b = y>0; % detection flag
map_y2p_full = 1+mod(0:(Nr-1),Npep_nz); % this is essentially the matrix B, but in index form. Again, non-detections are to be removed.

Nr_nz = sum(b); % only consider the nonzero entries in the constraints

% Build the A matrix. Note: the x vector is organized in the opposite way
% compared to what is shown in Fig. N2.2 in Mori et al. (2021):
% the first components are the efficiencies, and the other are the xTop
% intensities. Whoops!
cnt = 0;
for r=1:Nr
    if b(r)
        cnt=cnt+1;
        
        % A matrix
        vAr(2*cnt-1) = cnt;
        vAq(2*cnt-1) = 1+mod(r-1,Npep_nz);
        vAval(2*cnt-1) = 1;
        vAr(2*cnt) = cnt;
        vAq(2*cnt) = Npep_nz+1+floor((r-1)/Npep_nz);
        vAval(2*cnt) = 1;
        
        % mapping between constraint index r and peptide index p (indexed form)  
        map_y2p(cnt,1) = map_y2p_full(r);
        
        % To reconstruct the matrix B, notice that the entries of map_y2p
        % are the column index of B
    end
end
A = sparse(vAr,vAq,vAval,Nr_nz,Nq);
y = log(y(b)); % now this vector has length Ny_nz and contains log-transformed intensities


% To reconstruct the B matrix (in case it is needed)
vBr = 1:length(y);
vBq = map_y2p';
vBval = ones(1,length(y));
B = sparse(vBr,vBq,vBval,Nr_nz,Npep_nz); 

Ny_per_peptide = zeros(Npep_nz,1);
for p=1:(Npep_nz)
    Ny_per_peptide(p) = sum(map_y2p==p);
end

%% Setup peptide variances
% Setup variances (s2) and inverse variance diagonal matrix S.
% It is convenient to define a diagonal matrix S whose entries satisfy
% S_rr = \sum_p B_rp / s^2_p.
%
% The peptide variances are initialized randomly.
% Control the behavior of the randomization with the rng(seed) function
% (e.g. rng(1)), or use rng('shuffle') to inizialize the generator based on
% the current time.

S = speye(Npep_nz); % initialize matrix
s2 = ones(Npep_nz,1);

% Uncertainties for individual peptide precursors
for p=1:Npep_nz    
    s2(p) = (xTopSettings.s2_min)*(100+400*rand());
    v = find(map_y2p==p);
    for k=1:length(v)
        S(v(k),v(k)) = 1/s2(p);
    end
end

% Store the initial set of s2 for later
s2_init = s2;

% Print informations on A, B and S
if verbose>2
    [a,b] = size(A);
    fprintf('A matrix: %i rows, %i columns. Rank: %i\n',a,b,rank(full(A)));
    [a,b] = size(B);
    fprintf('B matrix: %i rows, %i columns. Rank: %i\n',a,b,rank(full(B)));
    [a,b] = size(S);
    fprintf('S matrix: %i rows, %i columns. Rank: %i\n',a,b,rank(full(S)));
    fprintf('Rank of A''*S*A: %i      Cond. number: %f\n',rank(full(A' *S*A)),cond(full(A' *S*A)));
end


%% Iterative procedure 

diff_L = 1; % just to get it through the first two while evaluations.
step = 0;

% For debug
for k=1:4
    xTop_output.analysis{k} = [];
end

% Add an extra constraint that plays the role of s2_soft / s2_hard in
% previous implementations. This constraint sets the average log(eff) to
% zero. (But really, any constraint on the efficiencies will work to fix
% the scale.)
extraConstraint = zeros(1,Nq);
extraConstraint(1:Npep_nz) = 1; % sum of all log-transformed efficiencies
extraConstraint = sparse(extraConstraint);

while (diff_L > xTopSettings.accuracy_max) && (step < xTopSettings.N_step_max)
    step = step + 1;
    
    % We solve this linear system by taking the minimum norm solution.
    % It is marginally faster than linsolve and can handle better
    % close-to-singular matrices than linsolve().
    
    M1 = [ A' * S * A  ; extraConstraint ];
    M2 = [ A' * S * y  ; 0]; % this "0" is arbitrary, we rescale the
                             % efficiencies at the end anyway
    
    % Check that the rank of the M1 matrix allows for a unique solution
    if rank(full(M1))<Nq        
        fprintf('\nNumber of variables: %i   (%i + %i)\n',Nq,Npep_nz,Nc_nz);
        fprintf('Rank of AtSA : %i\n',rank(full(A' * S * A)));
        fprintf('Rank of M1 = [AtSA;constraint] : %i\n',rank(full(M1)));
        fprintf('Rank of M2 = [AtSy;0] : %i\n',rank(full(M2)));
        error('Exiting.');
    end
    x = lsqlin( M1 , M2 ,[],[]);
    
    % Update variance matrix for the peptide intensities
    % For each peptide we recompute the standard deviation and adjust the
    % corresponding values of the S matrix
    %
    % Note that the entries of S corresponding to extra constraints are not
    % updated, since p only goes to Npep_nz and not to Npep_nz+2
    for p=1:Npep_nz
        iy = map_y2p==p;
        s2(p,1) = (xTopSettings.s2_min) + (sum((A(iy,:)*x - y(iy)).^2) / Ny_per_peptide(p));
        v = find(iy);
        for k=1:length(v)
            S(v(k),v(k)) = 1/s2(p);
        end
    end
    
    % For analysis plots
    if xTopSettings.plot_xTop_convergence
        x_vec{step} = x;
        s2_vec{step} = s2(1:Npep_nz);
    end
    
    % Value of the function L (-log(posterior probability))
    L_iter(step) = 0.5*((A*x-y)')*S*((A*x-y)) + 0.5*sum( Ny_per_peptide .* (((xTopSettings.s2_min)./s2) + log(s2)) );
    
    % Fractional difference with previous value
    if step>1
        diff_L = abs((L_iter(step)/L_iter(step-1)) - 1);
    end
    
    % Display info
    if verbose>2
        fprintf('[STEP %i]\n',step);
        for p=1:Npep_nz
            fprintf('  x(%i) = %f       s2(%i) = %f\n',p,x(p),p,s2(p));
        end
        fprintf('  <xTop> = %f\n',mean(x((Npep_nz+1):Nq)));
    end
    if verbose>2 && step>1
        fprintf('        -->    L = %f\n',L_iter(step));
        fprintf('        -->    log10(diff_L): %f\n',log10(diff_L));
        fprintf('               min(sigma) = %f\n',min(s2(1:Npep_nz)) );
        fprintf('               max(sigma) = %f\n',max(s2(1:Npep_nz)) );
    end
    
    % For debug
    xTop_output.analysis{1} = [ xTop_output.analysis{1}  x(Npep_nz+1) ]; % log I_1
    xTop_output.analysis{2} = [ xTop_output.analysis{2}  min(s2(1:Npep_nz)) ]; % minimum variance
    xTop_output.analysis{3} = [ xTop_output.analysis{3}  L_iter(step) ]; % difference between variables
    xTop_output.analysis{4} = [ xTop_output.analysis{4}  diff_L ]; % L difference
    
end

%% Wrap up the optimization and save relevant variables
if (diff_L <= xTopSettings.accuracy_max)
    xTop_output.optimization_status = 1;
elseif (step == xTopSettings.N_step_max)
    xTop_output.optimization_status = 2;
end
xTop_output.s2_init = s2_init;
xTop_output.N_step = step;
xTop_output.L = L_iter;
xTop_output.L_optimal = L_iter(step);
xTop_output.diff_L = diff_L;

if verbose>1
    fprintf('Optimization status: %2i     Steps: %3i     L : %10.5f   diff_L : %8.5e\n',...
        xTop_output.optimization_status,...
        xTop_output.N_step,...
        xTop_output.L_optimal,...
        xTop_output.diff_L);
end

%% Compute output from optimization results
% Peptide variance
s2_pep(valid_peptide_flag) = s2(1:Npep_nz);

% NFilter peptides for the absolute quantification
reliable_absQuant = true;
if xTopSettings.absolute_filter_ratio>0 && ...
   xTopSettings.absolute_filter_ratio<1
    
    % Valid peptides    
    s2_absThres = xTopSettings.s2_min ./ xTopSettings.absolute_filter_ratio;
    idxAbs = s2(1:Npep_nz) < s2_absThres;
    idx_absQuant_peptide = valid_peptide_flag & (s2_pep' < s2_absThres); % note that invalid peptides have s2_pep=0
    
    if sum(idx_absQuant_peptide)==0 % Oh well
        idx_absQuant_peptide = valid_peptide_flag;        
        reliable_absQuant = false;        
        idx_MaxEff = 1;
        max_eff = exp(x(idx_MaxEff));
    else
        v_x = x(1:Npep_nz);
        v_x(~idxAbs)=0;
        [max_eff,idx_MaxEff] = max(v_x);
    end
    
else
    idx_absQuant_peptide = valid_peptide_flag;
    idx_MaxEff = 1;
    max_eff = x(idx_MaxEff);
end


% xTop protein intensity. Note that if peptides are pre-sorted by average
% intensity, max_detection_efficiency should be equal to 1 (since that is
% the value of the efficiency of the first peptide). This is essentially arbitrary.
% If the top peptide is filtered out, then the max efficiency might be
% considerably smaller than one. In this case, the xTop intensity is
% lowered and all efficiencies are rescaled so that the maximum is 1.

% Rescale efficiencies and intensities
x(1:Npep_nz) = x(1:Npep_nz) - max_eff;
x((Npep_nz+1):Nq) = x((Npep_nz+1):Nq) + max_eff;

% Finally, the result
eff(valid_peptide_flag) = exp(x(1:Npep_nz) );
I_xTop(1,valid_condition_flag) = exp( x((Npep_nz+1):Nq) );


extraConstraint = zeros(1,Nq);
extraConstraint(idx_MaxEff) = 1;
M0 = full(A' * S * A);
M1 = [ M0 ; extraConstraint ];
C_vec =  diag(pinv(M1)) ; % this encodes the standard deviations for I_xTop and eff, assuming eff_max=1

log_eff_s2(valid_peptide_flag) = C_vec(1:Npep_nz); % error on the log(xTop efficiencies). Row vector
log_I_xTop_s2(valid_condition_flag) = C_vec((Npep_nz+1):Nq)'; % error on the log(xTop estimator). Row vector


% Set to zero very small inferred signals (mostly noise)
I_xTop(I_xTop<xTopSettings.thres_xTop) = 0;

% Assign xTop results to the output structure
xTop_output.I_xTop = I_xTop;
xTop_output.log_I_xTop_var = log_I_xTop_s2; % log-transformed -> relative error
xTop_output.eff = eff;
xTop_output.log_eff_var = log_eff_s2; % log-transformed -> relative error
xTop_output.pep_weight = 1./s2_pep; % weight of peptide in the definition of I_xTop
xTop_output.pep_weight(~valid_peptide_flag)=0;
xTop_output.relQuant_valid_flag = valid_peptide_flag; % peptides used for relative quantification
if reliable_absQuant
    xTop_output.absQuant_valid_flag = idx_absQuant_peptide; % peptides used for absolute quantification
else
    % unreliable quantification
    xTop_output.absQuant_valid_flag = false(size(idx_absQuant_peptide)); 
end

if xTopSettings.plot_xTop_convergence
    % For further analysis and debug. Updated most recently in v2.1
    
    figure('Renderer','Painters','Position',[400 400 500 500]);
    ah = subplot(2,2,1); hold on; box on;
    h1 = plot(ah,1:Npep_nz,log10(s2_init(1:Npep_nz)),'--','Color',[0 0 1]);
    for k=1:step
        x = (k-1)/(step-1);
        c = [0 0 0] + x*[1 0 0];        
        h2 = plot(ah,1:Npep_nz,log10(s2_vec{k}),'-','Color',c);
    end
    xlabel('Peptide index, p');
    ylabel('log_{10} \sigma^2_p');
    ylim( ylim() .* [0 1.1] + [log10(xTopSettings.s2_min) 0]);
    legend(h1,{'pre-start'});

    ah = subplot(2,2,2); hold on; box on;
    for k=1:step
        x = (k-1)/(step-1);
        c = [0 0 0] + x*[1 0 0];
        a = 1:Npep_nz;
        b = x_vec{k}(1:Npep_nz);
        i = ~isnan(b) & ~isinf(b);
        plot(ah,a(i),b(i),'-','Color',c);
    end
    xlabel('Peptide index, p');
    ylabel('log_{10} \epsilon_p');
    
    ah = subplot(2,2,3); hold on; box on;
    for k=1:step
        x = (k-1)/(step-1);
        c = [0 0 0] + x*[1 0 0];
        a = 1:Nc_nz;
        b = x_vec{k}((Npep_nz+1):Nq);
        i = b>0;
        plot(ah,a(i),b(i),'-','Color',c);
    end
    xlabel('Sample index, s');
    ylabel('log_{10} I_{xTop}');
    
    ah = subplot(2,2,4); hold on; box on;
    plot(ah,1:step,L_iter,'-','Color',[.1 .1 .1],'LineWidth',1);
    for k=1:step
        x = (k-1)/(step-1);
        c = [0 0 0] + x*[1 0 0];
        plot(ah,k,L_iter(k),'o','Color',c,'LineWidth',2,'MarkerFaceColor',c,'MarkerSize',4);
    end
    xlabel('Iteration step');
    ylabel('L (negative log-posterior)');
    
end

end


% The following function is used to export the data to plain text.
% Probably not the fastest, but hey, it works well for me.
% 
% The maintained version is available at:
%
% https://gitlab.com/mm87/export-dlm-data

function export_dlm_data(filename,data,varargin)
% function export_dlm_data(filename,data,varargin)
%
% Matteo Mori, February 28, 2019.
%
% This function offers flexible options to export tabular data to plain
% text (comma-delimited, tab-delimited, whathever).
%
% Mandatory arguments:
% 'filename': you know that
% 'data': data to be printed out. Several options are available:
%   - If 'data' is a matrix of floats, it is printed out as you would
%     expect (with either default format for floats or user-defined format)
%   - If 'data' is a cell matrix (more than 1 row and more than 1 column),
%     it is also printed out. If the user is not specifying the format,
%     defaults for floats and strings are used.
%   - If 'data' is a cell array (either 1 row or 1 column), then the
%   program assumes that each entry is an array (either cell or numeric)
%   containing the data to be printed in each column, unless the option
%   'force_array' is set to 1.
%
% Optional arguments:
% 'delimiter': string with the delimiter for the output file.
%              Examples: ',' for comma,'\t' for tab, ' ' for space
% 'header': cell array of strings specifying the top header of the table.
%           It can consist of multiple rows.
% 'maxRows': maximum number of rows printed to file.
% 'format': cell array of strings specifying the format for the different
%           columns
% 'numeric_format': default numeric format.
% 'string_format': default string format.
% 'force_array': flag (default is 0). If >0, and if 'data' is an array of
%               cells,the flag forces the program to print 'data' as it is.
%
% ---- EXAMPLES ----
%
% Say you want to export a numeric matrix M (nR rows and 3 columns) with
% an header, H={'col_1','col_2','col_3'} in a tab-delimited text
% file 'my_file.dat'. The first column has to be an integer, the other
% two columns are floating points between -10 and 10, fro be exported up to
% the 5th decimal digit. Then, one has to call:
%
% > export_dlm_data('my_file.dat',M,'delimiter','\t','header',H,...
%                    'format',{'%i','%8.5f','%8.5f'});
%
% If we want to include a column of strings S = {'str_1','str_2',...}
% in the first column, we can do the following:
%
% > X = {S,M(:,1),M(:,2),M(:,3)}; % cell array with 4 vectors
% > H = [{'header_str'},H]; % concatenate headers
% > export_dlm_data('my_file.dat',X,'delimiter','\t','header',H,...
%                    'format',{'%s','%i','%8.5f','%8.5f'});
%


numeric_format = '%f';
string_format = '%s';
delimiter = '\t';
top_header = {};
column_format = {};
maxRows = Inf;
force_array = 0;

%%%%%%%% optional arguments
Nin = length(varargin);
if Nin>0 && floor(Nin/2)<1
    error('Number of input arguments should be even');
else
    for i=1:2:(Nin-1)
        switch varargin{i}
            case {'numeric_format','numeric format'}
                numeric_format = varargin{i+1};                
            case {'string_format','string format'}
                string_format = varargin{i+1};                
            case {'delimiter','d'}
                delimiter = varargin{i+1};
            case {'top_header','header','h'}
                top_header = varargin{i+1};
            case {'maxRows','MaxRows','maxrows','Nrows'}
                maxRows = varargin{i+1};
            case {'format','column_format','colForm','col_form','col_format','f'}
                column_format = varargin{i+1};
            case {'force_array','force array'}
                force_array = varargin{i+1};
            otherwise
                error('Couple name-argument not recognized.');
        end
    end
end

fid = fopen(filename,'w');

%%%% Case 1: 'data' is a numerical matrix (floats)
if isfloat(data)
    [nR,nC]=size(data);
    nR = min(nR,maxRows);
    
    %%% format of data columns
    if isempty(column_format)        
        for i=1:nC
            column_format{i} = numeric_format;
        end
    end
    if abs(length(column_format)-nC)>0.5 
        error('Wrong length of column format cell array.');
    end
    
    % Top header? (string only)
    [rh,ch] = size(top_header);
    if ch==nC
        for j=1:rh
            for i=1:(nC-1)
                fprintf(fid,'%s',top_header{j,i});
                fprintf(fid,delimiter);
            end
            fprintf(fid,'%s',top_header{j,nC});
            fprintf(fid,'\n');
        end
    else
        error('Header has the wrong number of columns.');
    end
    
    % note that it only handle numbers!
    % sure you can convert to char...
    for r=1:nR
        for c=1:(nC-1)
            fprintf(fid,column_format{c},data(r,c));
            fprintf(fid,delimiter);
        end
        fprintf(fid,column_format{nC},data(r,nC));
        fprintf(fid,'\n');
    end
    
elseif iscell(data)
    [A,B]=size(data);
    if (A==1 || B==1) && (~force_array)
        % This is a collection of rows and columns to be printed.
        % Check sizes
        nC = max(A,B);
        % Maximum number of rows  (old version: nR = length(data{1})      
        nR = max(cellfun(@length,data));
        
        data_2 = cell(nR,nC);
        for c=1:nC
            if isfloat(data{c})
                for r=1:length(data{c})
                    data_2{r,c} = data{c}(r);
                end
            else
                for r=1:length(data{c})
                    data_2{r,c} = data{c}{r};
                end
            end
        end
        data = data_2;
        clear('data_2');
    
    elseif (A==1 || B==1) && force_array
        % transpose, so that 'data' is a column vector
        [A,B]=size(data);
        if A==1
            data = data';
        end
    end
    
    % This is a matrix of things to be printed
    [nR,nC] = size(data);
    nR = min(nR,maxRows);
    
    %%% format of data columns
    if isempty(column_format) 
        for c=1:nC
            if isfloat(data{1,c})
                column_format{c} = numeric_format;
            else
                column_format{c} = string_format;
            end
        end
    end
    if length(column_format) ~= nC 
        error('Wrong length of column format cell array.');
    end
    
    % Top header? (string only)
    [rh,ch] = size(top_header);
    if ch==nC
        for j=1:rh
            for i=1:(nC-1)
                fprintf(fid,'%s',top_header{j,i});
                fprintf(fid,delimiter);
            end
            fprintf(fid,'%s',top_header{j,nC});
            fprintf(fid,'\n');
        end
    else
        error('Header has the wrong number of columns.');
    end
    
    % print data
    for r=1:nR
        for c=1:(nC-1)
            fprintf(fid,column_format{c},data{r,c});
            fprintf(fid,delimiter);
        end
        fprintf(fid,column_format{nC},data{r,nC});
        fprintf(fid,'\n');
    end
end

fclose(fid);

end
        
        
        
    






